---
title: "Reproducing part of Brown et col. 2001"
author: "Christophe Pouzat"
date: "September 9 2022"
output:
       pdf_document:
         toc: false
       html_document:
         toc: false
urlcolor: orange	 
---

# Introduction

You are going to reproduce some figures and tables from the article by [Brown, Cai and DasGupta (2001)](https://repository.upenn.edu/cgi/viewcontent.cgi?article=1440&context=statistics_papers). I recommend (strongly) that you read at least the first 3 sections of the article, not only because it will be useful for answering the following questions but _especially_ because every statistician should be aware of the problems associated with confidence intervals of a [binomial distribution](https://en.wikipedia.org/wiki/Binomial_distribution).

# Some clues to answer the following questions

In the following, I will ask you to reproduce figures (or parts of figures) and tables from Brown's paper. All these figures and tables concern the _coverage probability_ of confidence intervals, i.e. the probability that the _true value_ of the parameter considered---here the probability of success---is contained in the interval. One must keep in mind that in most concrete cases, confidence intervals are obtained by asymptotic methods. The usual question then becomes, when we only have a finite sample size, whether the level of the interval (which will be referred to as _nominal probability_ in the article; understand by this, the probability that the true value of the parameter is included in the interval when the sample size tends to infinity) corresponds to the coverage probability. Most of your work will be devoted to calculating the coverage probability. I will now give you some hints on how to do it.

We consider a sample of size n drawn from a binomial distribution of parameter p. We will note X the number of successes. It is clear that given n and p you can (with the appropriate `R' function) obtain the P(X=i)---probability of X taking the value i---for i ranging from 0 to n. The "confidence interval procedures" considered in the article are functions of X and n (if it is easier for you, you can also say that these procedures generate two numbers: the two bounds of the interval). Since X is random, these functions are random too. Thus the answer to the question: "Is p contained in the interval?" which has only two possible answers "yes" or "no" can be seen as a random variable (as long as you code "yes" by 1 and "no" by 0). If you name this random variable IC(X,n;p), then the coverage probability becomes the expectation of this variable: $\sum_{i=0}^n IC(i,n;p), P(X=i)$.

# Your work

Redo:

- Figure 1
- Figure 2
- Table 1
- Figure 3
- The _Standard Interval_, _Agresti-Cull Interval_ and _Wilson Interval_ parts of Figure 5.


