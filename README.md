# Course material: Neuromod / MSC Modeling for Neuronal and Cognitive Systems Bootcamp

This repository contains material assciated to the lectures of [C Pouzat](https://xtof.perso.math.cnrs.fr/index.html) for the _Bootcamp_ of the Master [Modeling for Neuronal and Cognitive Systems](https://univ-cotedazur.eu/msc/modeling-for-neuronal-and-cognitive-systems), Université Côte d'Azur, 2022, 2023, 2024.

## Reproducible research

Folder `reproducible research` contains the files (`PDF`, `Rmd`, etc) associated to the first lecture on reproducible research.

## Fluorescence Data Analysis - The Case of Calcium Transients

Background material on the role of calcium ions in cellular/neuronal physiology (in French for now, sorry): [Analyse de transitoires calciques dans des neurones par mesures de fluorescence. I](https://drive.proton.me/urls/JEQYYP5MN0#oEgCslTV18LN). 
The overview slide show can be downloaded from [zenodo](https://doi.org/10.5281/zenodo.18691). The `Rmd` file can be obtained from the [fluorescence-data-analysis-the-case-of-calcium-transients-r-version](https://gitlab.com/c_pouzat/fluorescence-data-analysis-the-case-of-calcium-transients-r-version) `GitLab` repository in the `src` folder, the analysis description (`HTML` output of the former) is located at: [Fluorescence Data Analysis - The Case of Calcium Transients](https://c_pouzat.gitlab.io/fluorescence-data-analysis-the-case-of-calcium-transients-r-version/). 

If you are curious, a [`Python` version of the analysis](https://github.com/christophe-pouzat/ENP2015) as well as a [`C` version](https://github.com/christophe-pouzat/ENP2017) are available on `GitHub`.

## Some exercices in statistical analysis from Fig. 12 of Fatt & Katz (1952)

The `Rmd` file can be obtained from the [Inter MEPP Intervals with Fatt and Katz 1952](https://gitlab.com/c_pouzat/inter-mepp-intervals-with-fatt-and-katz-1952) `GitLab` repository in the `src` folder, the analysis description (`HTML` output of the former) is located at: [Inter MEPP Intervals with Fatt and Katz 1952](https://c_pouzat.gitlab.io/inter-mepp-intervals-with-fatt-and-katz-1952/). 
